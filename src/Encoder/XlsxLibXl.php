<?php

namespace Drupal\excel_libxl\Encoder;

use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Component\Utility\Html;
use Drupal\Core\Datetime\Entity\DateFormat;
use Symfony\Component\Serializer\Encoder\EncoderInterface;

/**
 * Adds XLSX encoder support via LibXL library for the Serialization API.
 */
class XlsxLibXl implements EncoderInterface {

  /**
   * The format that this encoder supports.
   *
   * @var string
   */
  protected static string $bookFormat = 'xlsx';

  /**
   * Format to write XLS files as.
   *
   * @var string
   */
  protected string $xlsFormat = 'Excel2007';

  /**
   * The XLSX Book default format.
   *
   * @var \ExcelFormat
   */
  protected $defaultFormat;

  /**
   * The XLSX Book columns formats array.
   *
   * @var \ExcelFormat[]
   */
  protected $formats;

  /**
   * The XLSX Book.
   *
   * @var \ExcelBook
   */
  protected $xlBook;

  /**
   * The XLSX Spreadsheet.
   *
   * @var \ExcelSheet
   */
  protected $spreadsheet;

  /**
   * Constructs an XLS encoder.
   *
   * @param string $xls_format
   *   The XLS format to use.
   */
  public function __construct(string $xls_format = 'Excel2007') {
    $this->xlsFormat = $xls_format;
  }

  /**
   * {@inheritdoc}
   */
  public function encode($data, $format, array $context = []) {
    switch (gettype($data)) {
      case 'array':
        // Nothing to do.
        break;

      case 'object':
        $data = (array) $data;
        break;

      default:
        $data = [$data];
        break;
    }

    try {
      // Instantiate a new excel object.
      $this->xlBook = new \ExcelBook(NULL, NULL, TRUE);
      $this->xlBook->setLocale('UTF-8');
      $this->spreadsheet = $this->xlBook->addSheet('Worksheet');
      $start_row = 0;
      // Add default book format.
      $this->defaultFormat = $this->xlBook->addFormat();
      $this->defaultFormat->horizontalAlign(\ExcelFormat::ALIGNH_LEFT);
      // Set header text.
      if (isset($context['header_text'])) {
        if (is_string($context['header_text'])) {
          $context['header_text'] = ['text' => $context['header_text']];
        }
        $this->setHeaderFooterText($context['header_text'], $context, $start_row);
      }
      // Set headers.
      $this->setHeaders($data, $context, $start_row);
      // Set the data.
      $this->setData($data, $start_row, $context);
      // Set footer text.
      if (isset($context['footer_text'])) {
        if (is_string($context['footer_text'])) {
          $context['footer_text'] = ['text' => $context['footer_text']];
        }
        $this->setHeaderFooterText($context['footer_text'], $context, $start_row);
      }
      // Do post actions.
      if (isset($context['post_actions']) && is_array($context['post_actions'])) {
        $this->doPostAction($context['post_actions'], $start_row);
      }

      if (!empty($context['views_style_plugin']->options['xls_settings'])) {
        $this->setSettings($context['views_style_plugin']->options['xls_settings']);
      }
      return $this->xlBook->save();
    }
    catch (\Throwable $e) {
      throw new InvalidDataTypeException($e->getMessage(), $e->getCode(), $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function supportsEncoding($format) {
    return $format === static::$bookFormat;
  }

  /**
   * Set sheet headers.
   *
   * @param array $data
   *   The data array.
   * @param array $context
   *   The context options array.
   * @param int $start_row
   *   The start row.
   */
  protected function setHeaders(array $data, array $context, &$start_row) {
    // TODO: Add configuration for table header.
    $font = $this->xlBook->addFont();
    $font->color(\ExcelFormat::COLOR_WHITE);
    $font->bold(TRUE);
    $font->size(12);
    $format = $this->xlBook->addFormat();
    $format->borderStyle(\ExcelFormat::BORDERSTYLE_THIN);
    $format->fillPattern(\ExcelFormat::FILLPATTERN_SOLID);
    $format->patternForegroundColor(\ExcelFormat::COLOR_OCEANBLUE_CF);
    $format->horizontalAlign(\ExcelFormat::ALIGNH_CENTER);
    $format->setFont($font);
    $c = 0;
    // Extract headers from the data.
    $headers = $this->extractHeaders($data, $context);
    foreach ($headers as $column => $header) {
      $this->spreadsheet->write($start_row, $c, $this->cleanValue($header), $format);
      $this->spreadsheet->setColWidth(0, $c, -1);
      $c++;
    }
    $start_row++;
  }

  /**
   * Set sheet header/footer text.
   *
   * @param array $data
   *   The data array.
   * @param array $context
   *   The context options array.
   * @param int $start_row
   *   The start row.
   */
  protected function setHeaderFooterText(array $data, array $context, &$start_row) {
    // TODO: Add configuration for table header.
    $font = $this->xlBook->addFont();
    $font->size(12);
    $format = $this->xlBook->addFormat();
    $format->setFont($font);
    if (!empty($data['skip_rows_before'])) {
      $start_row += $data['skip_rows_before'];
    }
    if (!empty($data['colspan'])) {
      $this->spreadsheet->setMerge($start_row, $start_row, 0, $data['colspan']);
    }
    $this->spreadsheet->write($start_row, 0, $this->cleanValue($data['text']), $format);
    if (($line_count = count(explode("\n", $data['text']))) > 1) {
      $default_height = $this->spreadsheet->rowHeight($start_row);
      $this->spreadsheet->setRowHeight($start_row, $line_count * $default_height, $format);
    }
    if (!empty($data['skip_rows_after'])) {
      $start_row += $data['skip_rows_after'];
    }
    $start_row++;
  }

  /**
   * Do sheet post action.
   *
   * @param array $actions
   *   The actions array.
   * @param int $start_row
   *   The start row.
   */
  protected function doPostAction(array $actions, &$start_row) {
    foreach ($actions as $key => $data) {
      switch ($key) {
        case 'split':
          $row = $data['row'] ?? 0;
          $column = $data['column'] ?? 0;
          if ($row || $column) {
            $this->spreadsheet->splitSheet($row, $column);
          }
          break;

        default:
      }
    }
  }

  /**
   * Set sheet data.
   *
   * @param array $data
   *   The data array.
   * @param int $start_row
   *   The start row.
   * @param array $context
   *   The context options array.
   */
  protected function setData(array $data, &$start_row, array $context) {
    $xls_data = $this->extractXlsServiceData($context);
    $data_start_row = $start_row;
    $fx_processing = count(array_column($xls_data, 'range_fx')) > 0;
    $data_keys = array_keys($data);
    $data_end_row = end($data_keys);
    foreach ($data as $i => $row) {
      $column = 0;
      $end_of_data = $i == $data_end_row;
      foreach ($row as $key => $value) {
        // TODO: Change behaviour: Type keys must be the same as data keys!!!
        $add_link = FALSE;
        $xlsx_column_data = $xls_data[$key] ?? [];
        $data_type = \ExcelFormat::AS_NUMERIC_STRING;
        // Since headers have been added, rows are offset here by start row.
        [$formatted_value, $format] = $this->formatValue($value, $column, $data_type, $xlsx_column_data);
        $type = $xlsx_column_data['type'] ?? '';
        if ($type === 'link') {
          $link_data = explode('|LINK|', $formatted_value);
          if (count($link_data) === 2) {
            $add_link = TRUE;
            [$formatted_value, $hyperlink] = $link_data;
          }
        }
        $this->spreadsheet->write($start_row, $column, $formatted_value, $format, $data_type);
        if ($add_link) {
          $this->spreadsheet->addHyperlink($hyperlink, $start_row, $start_row, $column, $column);
        }
        // Extra processing for the column range formula.
        if ($end_of_data && $fx_processing && isset($xlsx_column_data['range_fx'])) {
          $range_data = [
            $start_row,
            $data_start_row,
            $column,
            $xlsx_column_data['range_fx'],
          ];
          $processed_range_data = $this->preProcessRangeData($range_data);
          if (count($processed_range_data) == 3) {
            [$fx_start_row, $fx_value, $data_type] = $processed_range_data;
            $this->spreadsheet->write($fx_start_row, $column, $fx_value, $format, $data_type);
          }
        }
        $column++;
      }
      $start_row++;
      // Increase $start_row counter to avoid data loss.
      if ($end_of_data && $fx_processing) {
        $start_row++;
      }
    }
    if (isset($column)) {
      $col_width_data = array_column($xls_data, 'col_width', 'index');
      if (count($col_width_data) > 0) {
        for ($i = 0; $i <= $column; $i++) {
          $col_width = $col_width_data[$i] ?? -1;
          $this->spreadsheet->setColWidth(
            $i,
            $i,
            $col_width
          );
        }
      }
      else {
        $this->spreadsheet->setAutofitArea(0, $start_row, 0, $column);
      }
    }
  }

  /**
   * Helper function for preprocessing Range Data to write.
   *
   * @param array $data
   *   The range data array for handling.
   *
   * @return array
   *   The processed range data array.
   */
  protected function preProcessRangeData(array $data) {
    [$start_row, $data_start_row, $column, $range_fx] = $data;
    $fx_start_row = $start_row + 1;
    // Handle text values for the totals row.
    if (strpos($range_fx, 'text:') === 0) {
      $processed_data = [
        $fx_start_row,
        substr($range_fx, 5),
        \ExcelFormat::AS_NUMERIC_STRING,
      ];
    }
    else {
      $start_range_cell = $this->spreadsheet->rowColToAddr($data_start_row, $column);
      $end_range_cell = $this->spreadsheet->rowColToAddr($start_row, $column);
      $fx_value = "{$range_fx}({$start_range_cell}:{$end_range_cell})";
      $processed_data = [
        $fx_start_row,
        $fx_value,
        \ExcelFormat::AS_FORMULA,
      ];
    }
    return $processed_data;
  }

  /**
   * Clean value for a given XLSX cell.
   *
   * @param string $value
   *   The raw value to be cleaned.
   *
   * @return string
   *   The cleaned value.
   */
  protected function cleanValue($value) {
    $value = Html::decodeEntities($value);
    $value = strip_tags($value);
    $value = trim($value);

    return $value;
  }

  /**
   * Formats a single value for a given XLSX cell if type options provided.
   *
   * @param mixed $value
   *   The raw value to be formatted.
   * @param int $column
   *   The column index.
   * @param int $data_type
   *   The cell type.
   * @param array $xlsx_data
   *   (optional) The array of column options.
   *
   * @return array
   *   The formatted value with format.
   */
  protected function formatValue($value, $column, &$data_type, array $xlsx_data = []) {
    $need_changes = TRUE;
    if ($value === '' || $value === NULL) {
      return [$value, $this->defaultFormat];
    }
    try {
      $value = $this->cleanValue($value);
    }
    catch (\Throwable $throwable) {
      return ['Incorrect data type ' . gettype($value), $this->defaultFormat];
    }
    // Avoid adding formats multiply times.
    $new_format = FALSE;
    if (!isset($this->formats[$column])) {
      /** @var \ExcelFormat $format */
      $format = &$this->formats[$column];
      $format = $this->xlBook->addFormat();
      $format->horizontalAlign(\ExcelFormat::ALIGNH_LEFT);
      $new_format = TRUE;
    }
    // Simple conversion for Numeric fields.
    if ($need_changes && is_numeric($value)) {
      $need_changes = FALSE;
      $value = (float) $value;
    }
    $default_format = TRUE;
    // Deepest changes if Type exist.
    if ($need_changes && !empty($xlsx_data['type'])) {
      switch ($xlsx_data['type']) {
        case 'link':
          if ($new_format) {
            /** @var \ExcelFont $font */
            $font = $this->xlBook->addFont();
            $font->color(\ExcelFormat::COLOR_BLUE);
            $font->underline(\ExcelFont::UNDERLINE_SINGLE);
            $format->setFont($font);
          }
          $default_format = FALSE;
          break;

        case 'number':
          if ($new_format && !empty($xlsx_data['custom_cell_format'])) {
            $custom_cell_format = $this->xlBook->addCustomFormat($xlsx_data['custom_cell_format']);
            $format->numberFormat($custom_cell_format);
          }
          if ($this->parseNumeric($value, $xlsx_data)) {
            $default_format = FALSE;
          }
          break;

        case 'text':
          if ($new_format && !empty($xlsx_data['wrap_text'])) {
            $format->wrap(TRUE);
          }
          $default_format = FALSE;
          break;

        case 'date':
          if ($new_format) {
            $custom_cell_format = !empty($xlsx_data['custom_cell_format']) ? $xlsx_data['custom_cell_format'] : FALSE;
            $cell_format = $custom_cell_format ? $this->xlBook->addCustomFormat($custom_cell_format) : \ExcelFormat::NUMFORMAT_DATE;
            $format->numberFormat($cell_format);
          }
          if ($this->parseDate($value, $xlsx_data)) {
            $data_type = \ExcelFormat::AS_DATE;
            $default_format = FALSE;
          }
          break;

        case 'currency':
          if ($new_format) {
            $custom_cell_format = !empty($xlsx_data['custom_cell_format'])
              ? $xlsx_data['custom_cell_format']
              : '_("$"* #,##0.00_);_("$"* (#,##0.00);_("$"* "-"??_);_(@_)';
            $cell_format = $this->xlBook->addCustomFormat($custom_cell_format);
            $format->numberFormat($cell_format);
          }
          if ($this->parseCurrency($value, $xlsx_data)) {
            $default_format = FALSE;
          }
          break;
      }
    }
    $column_format = $default_format ? $this->defaultFormat : $this->formats[$column];
    return [$value, $column_format];
  }

  /**
   * Helper function for parsing Numeric values.
   *
   * @param mixed $value
   *   The value for parsing.
   * @param array $options
   *   The array of column options.
   *
   * @return bool
   *   The parsing result. True if parsing successful.
   */
  protected function parseNumeric(&$value, array $options) {
    $parsed_value = $value;
    $parsed_value = !empty($options['thousand_separator']) ? str_replace($options['thousand_separator'], '', $parsed_value) : $parsed_value;
    $parsed_value = !empty($options['decimal_separator']) ? str_replace($options['decimal_separator'], '.', $parsed_value) : $parsed_value;
    $parsed_value = preg_replace('/[^\d\.]/', '', $parsed_value);
    if (is_numeric($parsed_value)) {
      if (is_int($value)) {
        $value = (int) $parsed_value;
      }
      else {
        $value = (float) $parsed_value;
      }
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Helper function for parsing Date values.
   *
   * @param mixed $value
   *   The value for parsing.
   * @param array $options
   *   The array of column options.
   *
   * @return bool
   *   The parsing result. True if parsing successful.
   */
  protected function parseDate(&$value, array $options) {
    $parsed_value = $value;
    if (!empty($options['date_format']) && $date = \DateTime::createFromFormat($options['date_format'] . '|', $parsed_value)) {
      $value = $date->getTimestamp();
      return TRUE;
    }
    if ($timestamp = strtotime($parsed_value)) {
      $value = $timestamp;
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Helper function for parsing Currency values.
   *
   * @param mixed $value
   *   The value for parsing.
   * @param array $options
   *   The array of column options.
   *
   * @return bool
   *   The parsing result. True if parsing successful.
   */
  protected function parseCurrency(&$value, array $options) {
    $locale = !empty($options['locale']) ? $options['locale'] : 'en_US';
    $currency = !empty($options['currency']) ? $options['currency'] : 'USD';
    $parser = new \NumberFormatter($locale, \NumberFormatter::CURRENCY);
    $parsed_value = $parser->parseCurrency($value, $currency);
    if (!is_bool($parsed_value)) {
      $value = $parsed_value;
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Extract the headers from the data array.
   *
   * @param array $data
   *   The data array.
   * @param array $context
   *   The context options array.
   *
   * @return string[]
   *   An array of headers to be used.
   */
  protected function extractHeaders(array $data, array $context) {
    $headers = [];
    if ($first_row = reset($data)) {
      if (array_key_exists('views_style_plugin', $context)) {
        /** @var \Drupal\views\ViewExecutable $view */
        $view = $context['views_style_plugin']->view;
        $fields = $view->field;
        foreach ($first_row as $key => $value) {
          $headers[] = !empty($fields[$key]->options['label']) ? $fields[$key]->options['label'] : $key;
        }
      }
      elseif (array_key_exists('header', $context)) {
        $headers = $context['header'];
      }
      else {
        foreach ($first_row as $key => $value) {
          $headers[] = $key;
        }
      }
    }

    return $headers;
  }

  /**
   * Extract the Types from the Context array.
   *
   * @param array $context
   *   The context options array.
   *
   * @return string[]
   *   An array of headers to be used.
   */
  protected function extractXlsServiceData(array $context) {
    $xls_data = [];
    if (empty(!$context)) {
      if (array_key_exists('views_style_plugin', $context)) {
        /** @var \Drupal\views\ViewExecutable $view */
        $view = $context['views_style_plugin']->view;
        $fields = $view->field;
        /** @var \Drupal\views\Plugin\views\field\FieldPluginBase $field */
        foreach ($fields as $name => $field) {
          $plugin = $field->getPluginDefinition();
          // Try to process only simple entity fields.
          // TODO: Refactor processing.
          if ($plugin['id'] === 'field' && !empty($field->options['type'])) {
            switch ($field->options['type']) {
              case 'number_integer':
              case 'number_unformatted':
              case 'number_decimal':
              case 'number_float':
                $xls_data[$name] = [
                  'type' => 'number',
                ];
                if (!empty($field->options['settings']['thousand_separator'])) {
                  $xls_data[$name]['thousand_separator'] = $field->options['settings']['thousand_separator'];
                }
                if (!empty($field->options['settings']['decimal_separator'])) {
                  $xls_data[$name]['decimal_separator'] = $field->options['settings']['decimal_separator'];
                }
                break;

              case 'created':
              case 'timestamp':
              case 'datetime_default':
                $xls_data[$name] = [
                  'type' => 'date',
                ];
                if (!empty($field->options['settings']['format_type'])
                  && $date_type = DateFormat::load($field->options['settings']['format_type'])
                ) {
                  $xls_data[$name]['date_format'] = $date_type->getPattern();
                }
                elseif (
                  !empty($field->options['settings']['date_format'])
                  && $date_type = DateFormat::load($field->options['settings']['date_format'])
                ) {
                  $xls_data[$name]['date_format'] = $date_type->getPattern();
                }
                elseif (
                  !empty($field->options['settings']['custom_date_format'])
                ) {
                  $xls_data[$name]['date_format'] = $field->options['settings']['custom_date_format'];
                }
                break;
            }
          }
        }
      }
      elseif (array_key_exists('xls_data', $context)) {
        $xls_data = $context['xls_data'];
      }
    }

    return $xls_data;
  }

  /**
   * Set XLS settings from the Views settings array.
   *
   * @param array $settings
   *   An array of XLS settings.
   */
  protected function setSettings(array $settings) {
    $this->xlsFormat = $settings['xls_format'];
  }

}
