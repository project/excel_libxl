<?php

namespace Drupal\excel_libxl\Encoder;

/**
 * Adds XLS encoder support via LibXL library for the Serialization API.
 */
class XlsLibXl extends XlsxLibXl {

  /**
   * The format that this encoder supports.
   *
   * @var string
   */
  protected static string $bookFormat = 'xls';

  /**
   * Format to write XLS files as.
   *
   * @var string
   */
  protected string $xlsFormat = 'Excel5';

  /**
   * Constructs an XLS encoder.
   *
   * @param string $xls_format
   *   The XLS format to use.
   */
  public function __construct(string $xls_format = 'Excel5') {
    parent::__construct();
    $this->xlsFormat = $xls_format;
  }

}
